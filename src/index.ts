import assert from "assert";
import protobuf from "protobufjs";
import Long from "long";

protobuf.util.Long = Long;
protobuf.configure();

enum SubscribeType {
    Block = 'Block',
    Transaction = 'Transaction',
}

export interface IWsSender<T> {
    data_type: string;
    sender?: string;
    recipients: string[];
    payload: T;
}
export interface DbBlock {
    id: number;
    shard_id: number;
    block_level: number;
    block_hash: string;
    block_hash_previous: string;
    block_timestamp: number;
    producer: number;
    committee_id: number;
    transaction_count: number;
}
export interface DbTransaction {
    id: number;
    transaction_hash: string;
    shard_id: number;
    block_level: number;
    block_hash: string;
    block_timestamp: number;
    trx_signature: string;
    address_from: string;
    header_contract: string;
    header_payload_length: number;
    inner_nonce: number;
    inner_valid_to: number;
    payload_data: string;
}

interface IWsSenderProto {
    data_type: string;
    sender: Buffer;
    recipients: Buffer[];
    block?: Block;
    transaction?: Transaction;
}

interface Block {
    id: string;
    shard_id: number;
    block_level: string;
    block_hash: Buffer;
    block_hash_previous: Buffer;
    block_timestamp: string;
    producer: number;
    committee_id: number;
    transaction_count: number;
}

interface Transaction {
    id: string;
    transaction_hash: Buffer;
    shard_id: number;
    block_level: string;
    block_hash: Buffer;
    block_timestamp: string;
    trx_signature: Buffer;
    address_from: Buffer;
    header_contract: Buffer;
    header_payload_length: number;
    inner_nonce: number;
    inner_valid_to: string;
    payload_data: Buffer;
}

function postDecode(obj: IWsSenderProto): IWsSender<DbBlock> | IWsSender<DbTransaction> {
    switch (obj.data_type) {
        case SubscribeType.Block:
            assert(obj.hasOwnProperty('block'))
            const block = obj.block as Block;

            return {
                data_type: obj.data_type,
                recipients: obj?.recipients?.map(x => x.toString('hex')) || [],
                sender: obj?.sender?.toString('hex'),
                payload: {
                    block_hash: block.block_hash.toString('hex'),
                    block_hash_previous: block.block_hash_previous.toString('hex'),

                    id: Number(block.id),
                    shard_id: Number(block.shard_id),
                    block_level: Number(block.block_level),
                    block_timestamp: Number(block.block_timestamp),
                    producer: Number(block.producer),
                    transaction_count: Number(block.transaction_count),
                },
            } as IWsSender<DbBlock>

        case SubscribeType.Transaction:
            assert(obj.hasOwnProperty('transaction'))
            const transaction = obj.transaction as Transaction;
            return {
                data_type: obj.data_type,
                recipients: obj?.recipients?.map(x => x.toString('hex')) || [],
                sender: obj?.sender?.toString('hex'),
                payload: {
                    transaction_hash: transaction.transaction_hash.toString('hex'),
                    block_hash: transaction.block_hash.toString('hex'),
                    trx_signature: transaction.trx_signature.toString('hex'),
                    address_from: transaction.address_from.toString('hex'),
                    header_contract: transaction.header_contract.toString('hex'),
                    payload_data: transaction.payload_data.toString('hex'),

                    id: Number(transaction.id),
                    shard_id: Number(transaction.shard_id),
                    block_level: Number(transaction.block_level),
                    block_timestamp: Number(transaction.block_timestamp),
                    header_payload_length: Number(transaction.header_payload_length),
                    inner_nonce: Number(transaction.inner_nonce),
                    inner_valid_to: Number(transaction.inner_valid_to),
                },
            } as IWsSender<DbTransaction>


        default:
            throw new Error('Cannot deserialize protobuf')
    }

}

export class ProtoBuf {
    private wsSenderProto: protobuf.Type;
    private path: string;

    constructor(path?: string) {
        this.path = path;
    }
    async init() {
        const getRoot = (path: string = "structs.proto"): Promise<protobuf.Type> => {

            return new Promise((resolve, reject) => {
                // if from browser put in public folder
                const root = new protobuf.Root()
                root.load(path, { keepCase: true }, (err, root) => {
                    if (err) return reject(err)

                    const wsSenderProto = root.lookupType('structs.WsSenderProto')
                    resolve(wsSenderProto)
                })
            })
        }
        this.wsSenderProto = await getRoot(this.path)
    }
    decodeProtoBuffers(buf: Buffer | Uint8Array) {

        const bufToObject = (buf: Buffer | Uint8Array) => {
            const message = this.wsSenderProto.decode(buf)
            const object = this.wsSenderProto.toObject(message, { defaults: true, enums: String, longs: String }) as IWsSenderProto;
            return object
        }

        return postDecode(bufToObject(buf))
    }
}

// (async () => {
//     const x = new ProtoBuf();
//     await x.init()
//     let buf = Buffer.from('0801225308990418950422209076d4cbdedea72d983ca1338db091a55989acf771053143ad45cdedb9d6ca0f2a20b5fbf333b218cc593c312d884bffa43939501c59135a0271dbf0f4178ad088f43085beeba9ff2f3806', 'hex')
//     const object = x.decodeProtoBuffers(buf)
//     console.log(object)
// })()